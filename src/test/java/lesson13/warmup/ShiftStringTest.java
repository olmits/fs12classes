package lesson13.warmup;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShiftStringTest {

    private final ShiftString ss = new ShiftString();
    private final String orig = "Hello";

    @Test
    void shift1() {
        assertEquals("Hello", ss.shift(orig, 0, 0));
    }

    @Test
    void shift2() {
        assertEquals("elloH", ss.shift(orig, 1, 0));
    }

    @Test
    void shift3() {
        assertEquals("lloHe", ss.shift(orig, 2, 0));
    }

    @Test
    void shift4() {
        assertEquals("oHell", ss.shift(orig, 0, 1));
    }

    @Test
    void shift5() {
        assertEquals("Hello", ss.shift(orig, 0, 5));
    }

    @Test
    void shift6() {
        assertEquals("Hello", ss.shift(orig, 10, 5));
    }

    @Test
    void shift7() {
        assertEquals("oHell", ss.shift(orig, 1, 2));
    }

    @Test
    void shift8() {
        assertEquals("Hello", ss.shift(orig, 0, 1_000_000));
    }

}