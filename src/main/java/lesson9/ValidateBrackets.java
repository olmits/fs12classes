package lesson9;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * []
 * {}
 * **
 */

public class ValidateBrackets {

    private static Stack<Character> stack = new Stack<>();

    private static char openingBracket = '(';
    private static char closingBracket = ')';

    private static Map<Character, Character> bracketsMap;
    static {
        bracketsMap = new HashMap<>();
        bracketsMap.put('(', ')');
        bracketsMap.put('{', '}');
        bracketsMap.put('[', ']');
    }

    public boolean validate(String s) {
        if (s == null) return false;

        for (char c: s.toCharArray()) {
            switch (c) {
                case '(':
                case '{':
                case '[':
                    openingBracket = c;
                    closingBracket = bracketsMap.get(c);
                    stack.push(c);
                    break;
                case ')':
                case '}':
                case ']':
                    if (stack.isEmpty()) return false;
                    if (stack.size() > 1) return false;
                    stack.pop();
                    break;
                default: return false;
            }
        }

        if (closingBracket != bracketsMap.get(openingBracket)) return false;

        return stack.isEmpty();
    }
}
