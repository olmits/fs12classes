package lesson9;

import java.util.ArrayList;
import java.util.Comparator;

public class ComparableApp {
    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<Person>();
        people.add((new Person(1, "Jim", 20, 180)));
        people.add((new Person(2, "Alex", 40, 175)));
        people.add((new Person(3, "Sergio", 33, 160)));
        people.add((new Person(4, "Nate", 50, 155)));

        people.sort(new Comparator<Person>() {
            @Override
            public int compare(Person p1, Person p2) {
                int delta_age = p1.age - p2.age;
                if (delta_age != 0) return delta_age;

                return p1.name.compareTo(p2.name);
            }
        });
        people.forEach(System.out::println);
    }
}
