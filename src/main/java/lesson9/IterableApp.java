package lesson9;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class IterableApp {
    public static void main(String[] args) {
        int[] as = {1,2,3};
        for (int a:  as) {
            System.out.print(a);
        }

        List<String> nums = Arrays.asList("one", "two", "tree");
        for (String n : nums) {
            System.out.print(n);
        }

        Months months = new Months();
//        Iterator<String> it = months.iterator();
//        while (it.hasNext()) {
//            String m = it.next();
//            System.out.println(m);
//        }

        for (Iterator<String> it = months.iterator(); it.hasNext() ;) {
            String m = it.next();
            System.out.println(m);
        }

        for (String m: months) {
            System.out.print(m);
        }

        months.forEach(System.out::println);
    }
}
