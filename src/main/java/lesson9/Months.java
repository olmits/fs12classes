package lesson9;

import java.util.Iterator;

public class Months implements Iterable<String>{
    private final String[] data = {"Jan", "Feb", "Mar"};

    @Override
    public Iterator<String> iterator() {
        return new Iterator<String>() {
            private int pos = 0;
            @Override
            public boolean hasNext() {
                return pos < data.length;
            }

            @Override
            public String next() {
                return data[pos++];
            }
        };
    }
}
