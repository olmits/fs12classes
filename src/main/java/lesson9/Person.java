package lesson9;

public class Person {
    public final int id;
    public final String name;
    public final int age;
    public final int height;

    Person(int id, String name, int age, int height) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.height = height;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
