package lesson7.geometry;

class Square extends Figure {
    private int side;

    Square(int side) {
        this.side = side;
    }

    @Override
    public double area() {
        return side * side;
    }

    @Override
    public String toString() {
        return String.format("Square with Side=%d", side);
    }
}
