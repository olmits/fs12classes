package lesson7.geometry;

public class GeomApp {
    private static void print(String s) { System.out.print(s); }

    public static void main(String[] args) {
        Circle c = new Circle(5);
        Rectangle r = new Rectangle(5, 6);
        Square s = new Square(7);

        System.out.println(c.radius);
        System.out.println(c.area());
        System.out.println(c);
    }
}
