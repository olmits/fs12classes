package lesson7.geometry;

import java.text.MessageFormat;

class Circle extends Figure {
    final int radius;

    Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return String.format("Circle with Radius=%d", radius);
    }
}
