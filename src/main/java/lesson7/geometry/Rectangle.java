package lesson7.geometry;

class Rectangle extends Figure {
    private int width;
    private int height;

    Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double area() {
        return width * height;
    }

    @Override
    public String toString() {
        return String.format("Rectangle with Width=%d and Height=%d", width, height);
    }
}
