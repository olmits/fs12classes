package lesson7.geometry;

public abstract class Figure {
    public abstract double area();
}
