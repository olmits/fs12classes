package lesson5;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Test14 {

    private static void print(String s) {
        System.out.print(s);
    }

    private static List<Integer> popIndexedValueFromList(List<Integer> list, int index) {
        return IntStream
                .range(0, list.size())
                .filter(i -> i != index)
                .mapToObj(list::get)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private static void bonAppetit(List<Integer> bill, int k, int b) {
        List<Integer> newBill = popIndexedValueFromList(bill, k);
        int sumToPay = newBill.stream().mapToInt(Integer::intValue).sum();
        int sumOvercharged = b - sumToPay / 2;
        String result = sumOvercharged > 0 ? String.format("%d", sumOvercharged) : "Bon Appetit";
        print(result);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String[] nk = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(nk[0]);

        int k = Integer.parseInt(nk[1]);

        List<Integer> bill = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        int b = Integer.parseInt(bufferedReader.readLine().trim());

        bonAppetit(bill, k, b);

        bufferedReader.close();
    }
}
