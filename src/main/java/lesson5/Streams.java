package lesson5;

import java.util.Arrays;
import java.util.Collections;

public class Streams {

    private static void print(String a) {
        System.out.print(a);
    }

    private static int[][] split(int[] a) {

        int[] pe = {};
        for (int i:a) {
            if (i > 0 && i % 2 == 0) {
                pe = Arrays.copyOf(pe, pe.length + 1);
                pe[pe.length -1] = i;
            }
        }
        int[] po = {};
        for (int i:a) {
            if (i > 0 && i % 2 != 0) {
                po = Arrays.copyOf(po, po.length + 1);
                po[po.length -1] = i;
            }
        }
        int[] ne = {};
        for (int i:a) {
            if (i < 0 && i % 2 == 0) {
                ne = Arrays.copyOf(ne, ne.length + 1);
                ne[ne.length -1] = i;
            }
        }
        int[] no = {};
        for (int i:a) {
            if (i < 0 && i % 2 != 0) {
                no = Arrays.copyOf(no, no.length + 1);
                no[no.length -1] = i;
            }
        }

        return new int[][]{ pe, po, ne, no };
    }

//    private static int[][] split2(int[] a) {
//        int[] pe = Arrays.stream(a).reduce({}, );
//        int[] po = {};
//        int[] ne = {};
//        int[] no = {};
//
//
//
//        return new int[][]{ pe, po, ne, no };
//    }

    public static void main(String[] args) {
        int [] a = new int[] { -14, -1, -2, -1, 2, 4, 5, 7 };
        int [][] aSplited = split(a);

        for (int [] ass:aSplited) {
            System.out.print(String.format("%s\n", Arrays.toString(ass)));
        }
    }
}
