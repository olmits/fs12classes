package lesson6;

public class StudentApp {
    public static void main(String[] args) {
        Student s11 = new Student(1, "Tim");
        Student s21 = new Student(2, "Tim");
        Student s12 = new Student("Tim");
        Student s22 = new Student("Tim");
        System.out.println(s11);
        System.out.println(s21);
        System.out.println(s12);
        System.out.println(s22);
    }
}
