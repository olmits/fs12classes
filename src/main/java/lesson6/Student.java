package lesson6;

public class Student {
    private static int next = 0;
    private int id;
    private String name;

    Student(int id, String name) {
        this.name = name;
        this.id = id;
    }
    Student(String name) {
        this.name = name;
        this.id = ++next;
    }

    @Override
    public String toString() {
        return String.format("Student{id=%d, name='%s'}", id, name);
    }
}
