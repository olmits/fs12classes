package lesson13.main;

import lesson13.warmup.ShiftString;

public class lesson13 {

    private static ShiftString ss = new ShiftString();

    public static void main(String[] args) {
        String orig = "Hello";
        String test = ss.shift(orig, 1, 0);
        System.out.printf("%s\n", test);
    }
}
