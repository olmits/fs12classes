package lesson13.warmup;

public class ShiftString {
    public String shift(String orig, int left, int right) {
        int size = orig.length();
        int delta = (right - left) % size;

        if (delta > 0) {
            String l = orig.substring(0, orig.length() - delta);
            String r = orig.substring(size - delta);
            return r + l;
        } else if (delta < 0) {
            String l = orig.substring(0, -delta);
            String r = orig.substring(-delta);
            return r + l;
        } else return orig;
    }
}
