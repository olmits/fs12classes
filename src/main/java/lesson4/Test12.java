package lesson4;

import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import java.util.stream.IntStream;

public class Test12 {
    private static void print(String str) {
        System.out.print(str);
    }
    private static void staircase(int n) {
        IntStream.range(0, n).forEach(i -> {
            String spaces = String.join("", Collections.nCopies(n - 1 - i, " "));
            String sharps = String.join("", Collections.nCopies(i + 1, "#"));
            print(String.format("%s%s\n", spaces, sharps));
        });
    }


    private static void test(int n) {
        for (int i = 0; i < n; i++) {
            print(String.format("%" + n + "s\n", new String(new char[i + 1]).replace("\0", "#")));
        }
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        test(n);

        scanner.close();
    }
}
