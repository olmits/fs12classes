package lesson4;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import java.util.stream.IntStream;

public class Test11 {

    // Complete the plusMinus function below.
    private static void plusMinus(int[] arr) {
        double [] ratios = new double[3];

        ratios[0] = (double) (Arrays.stream(arr).filter(x -> x > 0).toArray().length) / arr.length;
        ratios[1] = (double) (Arrays.stream(arr).filter(x -> x < 0).toArray().length) / arr.length;
        ratios[2] = (double) (Arrays.stream(arr).filter(x -> x == 0).toArray().length) / arr.length;

        for (double r: ratios) System.out.print(String.format("%f\n", r));
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}
