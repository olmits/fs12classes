package lesson4;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Test13 {
    // Complete the dayOfProgrammer function below.
    private static boolean isGregorian(int year) {
        return year > 1918;
    }

    private static boolean isLeap(int year) {
        return isGregorian(year)
                ? (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
                : year % 4 == 0;
    }

    private static String dayOfProgrammer(int year) {
        int r = isLeap(year) ? (256 - 244) : (256 - 243);
        if (year == 1918) r += 13;
        return String.format("%d.09.%d", r, year);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int year = Integer.parseInt(bufferedReader.readLine().trim());

        String result = dayOfProgrammer(year);

        System.out.print(result);

//        bufferedWriter.write(result);
//        bufferedWriter.newLine();

        bufferedReader.close();
//        bufferedWriter.close();
    }
}
