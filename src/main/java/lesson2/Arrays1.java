package lesson2;

import java.util.*;

public class Arrays1 {

    /**
     * generates random number in range `from` to `to`
     * @param from int from is less than to
     * @param to int to
     * @return int
     */
    private static int get_random_int(int from, int to) {
        double r = Math.random(); // [0, 1)
        double rm = r * (to - from) + 1;
        return (int) (Math.round(rm));
    }

    private static int[] random_ints(int n, int from, int to) {
        System.out.printf("n: %d, from: %d, to: %d%n", n, from, to);
        int[] result_arr = new int[n];
        for (int i = 0; i < result_arr.length; i++) {
            int random_int = get_random_int(from, to);
            result_arr[i] = random_int;
        }
        return result_arr;
    }

    /**
     *
     * @param arr - source array
     * @return minimal item of the array
     * or NoSuchElementException if array is empty
     */
    private static int array_min(int[] arr) {
        if (arr.length == 0) throw new NoSuchElementException("Given array is empty");
        int min = Integer.MAX_VALUE;
        for (int value : arr) min = Math.min(value, min);
        return min;
    }

    static void arraysSyntax() {
        int[] a = { 1, 2, 3};
    }

    public static void main(String[] args) {
        int x_n = get_random_int(1, 100);
        int x_from = get_random_int(1, 10);
        int x_to = get_random_int(100, 1000);

        int[] arr_x = random_ints(x_n, x_from, x_to);

        System.out.println(Arrays.toString(arr_x));

        int arr_x_min = array_min(arr_x);

        System.out.print(arr_x_min);

    }
}
