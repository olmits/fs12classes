package lesson2;

public class Rectangle {

    private static void print(String arg) {
        System.out.print(arg);
    }

    private static boolean isSide(int i, int j, int h, int w) {
        return i == 0 || j == 0 || i == h - 1 || j == w - 1;
    }

    private static boolean isDiagonal(int i, int j, int h, int w) {
        double k = (double) (w) / h;
        int ik = (int) (i * k);
        int i_reverted = w - ik - 1;
        return ik == j || i_reverted == j;
    }

    private static boolean isVisible(int i, int j, int h, int w) {
        return isSide(i, j, h, w) || isDiagonal(i, j, h, w);
    }

    public static void main(String[] args) {
        int W = 10;
        int H = 10;

        for (int i = 0; i < H; i++) {
            for (int j = 0; j < W; j++) {
                print(isVisible(i, j, H, W) ? "*" : " ");
            }
            print("\n");
        }
    }
}
