package lesson2;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Test8 {

    private static int B = 0;
    private static int H = 0;
    private static boolean flag = false;

    private static Scanner scanner = new Scanner(System.in);

    static  {
        try {
            B = scanner.nextInt();
            H = scanner.nextInt();
            flag = B > 0 && H > 0;
            if (!flag) {
                throw new Exception("Breadth and height must be positive");
            }
        } catch (Exception e) {
            System.out.print(e);
        }
    }


    public static void main(String[] args){
        if(flag){
            int area=B*H;
            System.out.print(area);
        }
    }
}
