package lesson2;

import java.util.*;
import java.io.*;

public class Test5 {

    private static void print(int r) {
        System.out.printf("%d ", r);
    }

    public static void main(String []arch){
        Scanner in = new Scanner(System.in);
        int t=in.nextInt();
        for(int i=0;i<t;i++){
            int a = in.nextInt(); // 5
            int b = in.nextInt(); // 3
            int n = in.nextInt(); // 5

            for (int j = 0; j < n; j++) {
                int result = a;
                for (int k = 0; k < j + 1; k++) {
                    result += (int)(Math.pow(2, k)) * b;
                }
                print(result);
            }
            System.out.print("\n");
        }
        in.close();
    }
}
