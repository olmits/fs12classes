package lesson2;

import java.util.Scanner;

public class Test7 {

    public static void main(String[] args) {
        String line;
        int i = 0;
        Scanner scanner = new Scanner((System.in));
        while (scanner.hasNextLine() && !( line = scanner.nextLine() ).equals( "" )) {
            i++;
            System.out.printf("%d. %s%n", i, line);
        }

        scanner.close();
    }
}
