package lesson2;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Test4 {

    private static final Scanner scanner = new Scanner(System.in);

    private static void countAndPrintResult(int n, int i) {
        int result = n * i;
        System.out.printf("%d x %d = %d%n", n, i, result);
    }

    public static void main(String[] args) {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 10; i++) {
            countAndPrintResult(N, i + 1);
        }

        scanner.close();
    }
}
