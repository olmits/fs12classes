package lesson8;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Test18 {
    // Complete the compareTriplets function below.
    private static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        ArrayList<Integer> ratings =
                (ArrayList<Integer>) IntStream
                        .range(0, a.size())
                        .map(i -> (a.get(i) - b.get(i)))
                        .filter(j -> j != 0)
                        .boxed()
                        .collect(Collectors.toList());


        System.out.print(a.toString());
        System.out.print(b.toString());
        System.out.print("\n");
        System.out.print(ratings.toString());
        System.out.print("\n");

        int aCount = (int) ratings.stream().filter(x -> x > 0).count();
        int bCount = (int) ratings.stream().filter(x -> x < 0).count();

        return Arrays.asList(aCount, bCount);
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        List<Integer> a = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        List<Integer> b = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        List<Integer> aa = Arrays.asList(17, 28, 30);
        List<Integer> bb = Arrays.asList(99, 16, 8);

        List<Integer> result = compareTriplets(aa, bb);

        System.out.print(result.toString());

        bufferedWriter.write(
                result.stream()
                        .map(Object::toString)
                        .collect(joining(" "))
                        + "\n"
        );

        bufferedReader.close();
        bufferedWriter.close();
    }
}
